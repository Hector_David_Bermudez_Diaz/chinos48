<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo Artista</title>
</head>
<body>
<form class="form-horizontal" method="POST" action="{{ url('artistas/store')}}">
@csrf

<fieldset>

<!-- Form Name -->
<legend>Nuevo Artista</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nombre Artista/Banda</label>  
  <div class="col-md-4">
  <input id="textinput" name="nombre_artista" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
    <button type="submit" id="" name="" class="btn btn-success">Enviar</button>
  </div>
</div>

</fieldset>
</form>

<!--Mensaje: En caso de qe exista-->
@if(session('exito'))
  <p class="aler-success">{{session("exito")}}</p>
  <p class="text-danger">Artista creado <strong>{{session("nombre_artista")}} </strong></p>

  @else <!--Hay errores de validacion-->
  @foreach($errors->all() as $error)
    <p class="alert-danger"> {{$error}} </p>
  @endforeach

@endif

</body>
</html>
@extends('layouts.masterpage')

@section('contenido')
    <h1>Lista de Empleados</h1>
    <table class="table table-borderless">
       <thead>
     <tr>
       <th>Nombre y Apellidos</th>
       <th>Cargo</th>
    <!--   <th>Fecha nacimiento</th>
       <th>Fecha contratacion</th>
       <th>Direccion</th>
       <th>Ciudad</th>-->
       <th>Email</th>
      <!-- <th>Jefe inmediato</th> -->
      <th class="text-info">Ver detalles</th>
     </tr>
       </thead>
        <tbody>
        @foreach($empleados as $empleado)
         <tr>
            <td><strong class="text-danger">{{ $empleado->FirstName }} {{ $empleado->LastName }}</stong></td>
            <td><strong class="text-danger">{{ $empleado->Title }}</stong></td>
    <!--        <td><strong class="text-danger">{{ $empleado->BirthDate }}</stong></td>
            <td><strong class="text-danger">{{ $empleado->HireDate }}</stong></td>
            <td><strong class="text-danger">{{ $empleado->Address }}</stong></td>
            <td><strong class="text-danger">{{ $empleado->City }}</stong></td> -->
            <td><strong class="text-danger">{{ $empleado->Email }}</stong></td>
     <!-- <td><strong class="text-danger">{{ $empleado->ReportsTo }}</stong></td> -->
            <td><a class="btn btn-primary" href="{{ url('empleados/'.$empleado->EmployeeId) }}"> Ver información </a></td>
         </tr>
         
        @endforeach
        </tbody>

       
    </table>
    {{ $empleados->links()}}

@endsection